import java.util.Scanner;

public class rFibo {
  static int rFibo(int n) {
    if (n == 1){
      return 0;
    }
    if (n == 2) {
      return 1;
    }
    else {
      return rFibo(n - 1) + rFibo(n - 2);
    }
  }

  public static void main(String[] args) {
    Scanner console = new Scanner(System.in);
    int n = console.nextInt();
    for (int i = 0; i <= n; i++) {
      System.out.println(rFibo(n));
    }
  }
}
