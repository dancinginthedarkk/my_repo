import java.util.Scanner;

public class Fibo {
  public static void main(String[] args) {
    Scanner console = new Scanner(System.in);
    int n = console.nextInt(); // количество чисел
    int a = 1;
    int b = 1;
    if (n == 1) {
      System.out.print(a);
    }
    if (n == 2) {
      System.out.print(a + " " + b);
    }
    else {
      int result = 0;
      int f1 = 1;
      int f2 = 1;
      System.out.print(a + " " + b);
      for (int i = 2; i < n; i++) {
        result = f1 + f2;
        System.out.print(" " + result);
        f1 = f2;
        f2 = result;
      }
    }
  }
}
