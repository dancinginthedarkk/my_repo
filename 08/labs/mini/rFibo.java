import java.util.Scanner;

public class rFibo {

  public static void main(String[] args) {
    // rFibo(6);
    Scanner console = new Scanner(System.in);
    int n = console.nextInt();
    for (int i = 0; i < n; i++) {
      System.out.println(rFibo(i));
    }
  }

  public static int rFibo(int n) {
    if (n == 0){
      return 0;
    }
    if (n == 1) {
      return 1;
    }
    else {
      return rFibo(n - 1) + rFibo(n - 2);
    }
  }
}
