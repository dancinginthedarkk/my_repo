package labs.hard;
import java.util.Scanner;
import java.util.Date;
import java.util.Random;
/*
Создать программу, моделирующую случайный процесс.
С клавиатуры задается некий "порог" в виде вещественного числа,
коэффициент влияния порога и максимальное время моделирования.
В случайные моменты времени генерируются случайные вещественные
числа в интервале [-коэффициент*порог; коэффициент*порог].
Если сгенерированное число больше заданного порога, то прекратить
моделирование. По завершении моделирования вывести следующую
информацию на консоль: <время от начала эксперимента>: <число>
*/

public class Lab4{
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    System.out.print("Введите порог: ");
    double porog = sc.nextDouble(); //порог

    System.out.print("Введите коэффициент: ");
    double coeff = sc.nextDouble(); //коэффициент влияния порога

    System.out.print("Введите максимальное время: ");
    long max_time = sc.nextLong(); //максимальное время моделирования

    Thread thread = new Thread();
    double counter = Math.random()*(2*(porog*coeff)) - (porog*coeff); //интервал [-коэффициент*порог; коэффициент*порог]

    long start = System.currentTimeMillis();
    long end = start;
    boolean flag = true;

    while (flag){
      if (counter <= porog && ((end - start) <= max_time)) {
        try{
          thread.sleep(new Random().nextInt(10));
        }
        catch (Exception e){}
        end = System.currentTimeMillis();
      }
      else{
        flag = false;
      }
      System.out.println("" + counter + " " + (end - start));
    }
  }
}
