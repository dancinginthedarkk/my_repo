package labs.hard;
import java.util.Scanner;

/*
Создать программу, позволяющую вводить с клавиатуры число,
сохранять его в целочисленной переменной и затем выводить
переменную на консоль. Надо учесть, что пользователь может
ввести любую строку. Если среди значащих символов есть только
цифры, + или -, то строка должна быть преобразована в число.
*/

public class Lab1 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Введите число:");
    int n;
    if (sc.hasNextInt()) {
      n = sc.nextInt();
      System.out.println("Ваше число: " + n);
    }
    else {
      System.out.println("Ввведите строку, состоящую ТОЛЬКО из цифр и '+' или '-'");
    }
    sc.close();
  }
}
