package labs.hard;
import java.util.Scanner;
import java.util.Date;

/*
Пользователь вводит с клавиатуры количество чисел.
Программа должна случайным образом генерировать введенное
количество чисел и выводить время генерации. Если время
генерации больше 5 секунд, то выводить сообщение "превышено
допустимое время работы программы, введите количество поменьше".
Если время генерации меньше 5 секунд, то выводить
сгенерированные числа в консоль через запятую.
*/

public class Lab2 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int[] arr = new int[n];

    long start = System.currentTimeMillis();

    for (int i = 0; i < arr.length; i++) {
      arr[i] = (int) (Math.random() * 1000);
    }
    long end = System.currentTimeMillis();

    long time = (end - start) / 1000;

    if (time < 5) {
      for (int i = 0; i < arr.length; i++) {
         System.out.print(arr[i] + ", ");
      }
    }
    else{
      System.out.println("Превышено допустимое время работы программы, введите количество поменьше!");
    }
      System.out.println();

  }
}
