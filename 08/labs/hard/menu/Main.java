package labs.hard.menu;

import java.util.Scanner;

class Main{
  public static void main(String[] args) {
    Question zero = new Question();
    Question first = new Question();
    Question second = new Question();
    Question third = new Question();
    Action a0 = new Action_0("Вывести приветствие");
    zero.connect(first);
    zero.setAction(a0);

    first.setAction(new Action_1("Вывести кое-что"));
    first.connect(second);

    second.setAction(new Action_2("Вывести кое-что"));

    third.setAction(new Action_3("Вывести кое-что"));

    menuStart(zero);
  }

  public static void menuStart(Question start) {
    Scanner scan = new Scanner(System.in);
    int answ = 0;
    boolean flag = true;
    while(flag){
      start.printDescription();
      answ = scan.nextInt();
      if (answ == 0){
         if (start.parent == null){
              flag = false;
        } else {
          start = start.parent;
        }
      }
      if(answ > 0 && answ <= start.actions.size()){
        start.actions.get(answ - 1).act();
        System.out.println("1");
      }
      if(answ > start.actions.size() && answ <= start.actions.size() + start.children.size()) {
          start = start.children.get(answ - start.children.size());
          System.out.println("2");
      }
      if(answ > start.actions.size() + start.children.size()){
          System.out.println("Введите корректное значение");
      }
    }
  }
}
