import java.util.Scanner;

public class Menu_2{
    private static int level = 1;

    public static void main(String[] args) {
      System.out.println("Добро пожаловать в меню!");
      System.out.println("Выберите пункт:" + "\n"
                      + "0. Выход" + "\n"
                      + "1. Вывести кое-что" + "\n"
                      + "2. Следующий уровень"
      );
        Scanner scanner = new Scanner(System.in);

        while (level > 0 && level < 4) {
            int request = scanner.nextInt();
            menu(request);
            if (request == 0){
              break;
            }
        }
    }

    private static void menu(int request){
      String[] arr = new String[]{("Вы на "+ level + "уровне! Выберите пункт меню:" + "\n"
                      + "0. Выход" + "\n"
                      + "1. Вывести кое-что" + "\n"
                      + "2. Следующий уровень"
      )};
      switch (request){
        case 0:
          System.out.println("Bye bye!");
          break;
        case 1:
          switch (level) {
            case 1:
              System.out.println("Тут зеленый динозаврик!");
              break;
            case 2:
              System.out.println("Тут желтый динозаврик!");
              break;
            case 3:
              System.out.println("Ну все, динозаврики закончились!");
              break;
          }
          break;
        case 2:
          level += 1;
          System.out.println("Вы на " + level + " уровне. Выберите пункт меню:" + "\n"
                          + "0. Выход" + "\n"
                          + "1. Вывести кое-что" + "\n"
                          + "2. Следующий уровень"
          );
          if (level == 4){
            System.out.println("Вы на " + level + " уровне. Это все!" + "\n");
          }
    }
  }


}
