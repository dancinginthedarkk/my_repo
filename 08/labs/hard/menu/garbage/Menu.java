import java.util.Scanner;
import java.util.Date;

/*
Создать программу, предоставляющую пользователю трехуровневое меню.
В каждом уровне есть пункт для возвращения на предыдущий уровень
(на нулевом уровне - выход), хотя бы один пункт для перехода на
уровень ниже (кроме 3 уровня) и хотя бы 1 пункт для выполнения
некоторого действия (вывод на экран строки).
*/

class Menu{
  public static void main(String[] args) {
    menu(0);
  }

  public void menu(int level){
    if (level == 0){
      System.out.println("Вы в меню. Выберите вариант:");
      System.out.println("0. Выход");
      System.out.println("1. Вывести зеленого динозаврика");
      System.out.println("2. Следующий уровень");
      Scanner sc = new Scanner(System.in);
      int answer = sc.nextInt();
      if (answer == 0){
        return 0;
      }
      if(answer == 1){
        menu(1);
      }
      if(answer == 2){
        menu(2);
      }
    }
    if (level == 1){
      System.out.println("Зеленый динозаврик!");
      System.out.println();
      menu(0);
    }
    if (level == 2){
      System.out.println("Вы в меню. Выберите вариант:");
      System.out.println("0. Назад");
      System.out.println("1. Вывести желтого динозаврика");
      System.out.println("2. Следующий уровень");
      answer = sc.nextInt();
      if(answer == 0){
        menu(0);
      }
      if(answer == 1){
        menu(3);
      }
      if(answer == 2){
        menu(4);
      }
    }
    if (level == 3){
      System.out.println("Желтый динозаврик!");
      System.out.println();
      menu(2);
    }
    if (level == 4){
      System.out.println("Вы в меню. Выберите вариант:");
      System.out.println("0. Назад");
      System.out.println("1. Вывести еще динозавриков");
      System.out.println("2. Выход");
      answer = sc.nextInt();
      if(answer == 0){
        menu(2);
      }
      if(answer == 1){
        menu(5);
      }
      if(answer == 2){
        return 0;
      }
    }
    if (level == 5){
      System.out.println("Динозаврики закончились");
      System.out.println();
      menu(4);
    }
    return 0;
  }
}
