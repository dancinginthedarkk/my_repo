package labs.hard.menu;

abstract public class Action{
  String title;

  Action(String title){
    this.title = title;
  }

  abstract public void act();

  public String getTitle(){
    return this.title;
  }

}
