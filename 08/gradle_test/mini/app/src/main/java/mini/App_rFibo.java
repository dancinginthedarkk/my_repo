package mini;

public class App_rFibo {
    public static int[] rFibo(int a){

        int first_num = 0;
        int sec_num = 1;
        int[] result = new int[a];
        if (result.length > 2){
          result[0] = 0;
          result[1] = 1;
          for (int i = 2; i < a; i++){
            int temp = first_num + sec_num;
            result[i] = temp;
            first_num = sec_num;
            sec_num = temp;
          }
          return result;
        }
        if (result.length == 2){
          return new int[] {0, 1};
        }else {
          return new int[]{0};
        }
    }

  public static void main(String[] args){
		System.out.println(rFibo(5));
  }
}
