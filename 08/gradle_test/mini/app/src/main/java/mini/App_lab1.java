package mini;

public class App_lab1 {
//фибо без рекурсии
  public static int[] Fibo(int[] arr){
    int[] arr1 = new int[6];
    arr1[0] = 1;
    arr1[1] = 1;
    for (int i = 2; i < arr1.length; i++) {
      arr1[i] = arr1[i - 1] + arr1[i - 2];
    }
    for (int i = 0; i < arr1.length; i++) {
      System.out.print(arr1[i] + ", ");
    }
    return arr1;
  }

  public static int Factorial(int n){
    int a = 3;
    int counter = 1;
    for (int i = 1; i <= a; i++){
      counter = counter*i;
    }
    return counter;
  }

  public static int rFactorial(int n) {
    if (n == 1){
      return 1;
    }
    else {
      return n * rFactorial(n - 1);
    }
  }

  public static int summa(int n) {
    int counter = n * (n + 1) / 2;
    return counter;
  }

  public static int stroka(String name, char result) {
  char[] chara = name.toCharArray();
  int counter = 0;
  for (int i = 0; i < name.length(); i++) {
    if (chara[i] == 'a') {
      counter++;
    }
  }
  return counter;
  }
}
