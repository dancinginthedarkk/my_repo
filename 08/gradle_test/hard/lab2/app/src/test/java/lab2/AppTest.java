package lab2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
  @Test void test_ok() {
    Lab2 generator = new Lab2(100, 5);
    assertTrue(generator.getSucsessful());
    assertEquals(100, generator.getResult().length);
  }

  @Test void test_notok(){
    Lab2 generator = new Lab2(1234567, 5);
    assertNull(generator.getResult());
  }
}
