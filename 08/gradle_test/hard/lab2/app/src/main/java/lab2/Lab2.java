package lab2;

import java.util.Random;
import java.util.Date;
/*
Пользователь вводит с клавиатуры количество чисел.
Программа должна случайным образом генерировать введенное
количество чисел и выводить время генерации. Если время
генерации больше 5 секунд, то выводить сообщение "превышено
допустимое время работы программы, введите количество поменьше".
Если время генерации меньше 5 секунд, то выводить
сгенерированные числа в консоль через запятую.
*/

public class Lab2 {

  private int[] nums;
  private long timeToGenerate;
  private long timePorog; //5 секунд
  private int count;
  private boolean successful;


  Lab2(int count, int timePorog){
    this.count = count;
    this.nums = new int[this.count];
    this.timePorog = timePorog;
  }

  private void generate(){

    long start = new Date().getTime();

    for (int i = 0; i < this.count; i++){
      this.nums[i] = new Random().nextInt(150);
    }

    this.timePorog = new Date().getTime() - start;
  }

  public boolean getSucsessful(){
    return (this.timeToGenerate <= this.timePorog);
  }

  public int[] getResult(){
    (this.getSucsessful()) ? this.nums : null;
  }

  private String failed(){
    return "Превышено ограничение по времени";
  }
}
