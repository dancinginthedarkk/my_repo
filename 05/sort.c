#include <stdint.h>
#include <stdio.h>

int main() {
	int massive[10] = {1, 7, 34, 6, 32, 12, 5, 8, 4, 9};
	int i = 0;
	int s = 0;
	int n = 10;
	while ( i != (n - 1) ) {
		if (massive[i] > massive[i + 1]) {
			s = massive[i + 1];
			massive[i + 1] = massive[i];
			massive[i] = s;
			i = 0;
		} else {
			i+=1;
		}
  	}
  	for (i = 0; i < n; i++){
        printf("%d ", massive[i]);
  	}
}
